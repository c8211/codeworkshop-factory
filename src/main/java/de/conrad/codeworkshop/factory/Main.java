package de.conrad.codeworkshop.factory;

import static org.springframework.boot.SpringApplication.run;


public class Main {
    public static void main(final String... args) {
        run(FactoryApplication.class);
    }
}
